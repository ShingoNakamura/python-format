# python lint/format するリポジトリ

サンプルに使った[元のリポジトリ](https://github.com/keon/algorithms)、[元々のREADME](./README.origin.md)


## 使い方(CLI)

VSCodeでの設定は[Googleドキュメントシート](https://docs.google.com/document/d/1oPlJWJVjGlXnOodgdN2iAoRWzSfLWZVFNWUOyzk41Uk/edit)を参照

### インストール

```
pip install -r requirements.txt
```

### lintをかける

```
flake8
```

### formatをかける

ドライランを事前に行うか、フォーマット後は `git diff` 等で変更を目視で確認すること

```
autopep8 -dvr . # dry run
autopep8 -ivr .
```

※ auto formatですべてのlint errorを解消できるわけではない。具体的に解消できるルールは[ここを参照](https://github.com/hhatto/autopep8#features)


## 注意点

flake8は命名規則をチェックしない。  
[pep8-naming 0.8.2](https://pypi.org/project/pep8-naming/)があるが、少し怪しいので様子見(試せたら試す)