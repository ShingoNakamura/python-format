import math
import sys


def example1():
    # This is a long comment. This should be wrapped to fit within 72 characters.
    some_tuple = (1, 2, 3, 'a')
    some_variable = {
        'long': 'Long code lines should be wrapped within 79 characters.',
        'other': [
            math.pi,
            100,
            200,
            300,
            9876543210,
            'This is a long string that goes on'],
        'more': {
            'inner': 'This whole logical line should be wrapped.',
            some_tuple: [
                1,
                20,
                300,
                "hogehogeaaaaaaaaaaaaaaaaaaaaaaa"
                "zzzzzzzzzzzzzzzzzzzzz",
                40000,
                500000000,
                60000000000000000
                ]
            }
        }
    return (some_tuple, some_variable)


def example2():
    return ('' in {'f': 2}) in {'has_key() is deprecated': True}


class Example3(object):
    def __init__(self, bar):
        # Comments should have a space after the hash.
        if bar:
            bar += 1
            bar = bar * bar
            return bar
        else:
            some_string = """
                       Indentation in multiline strings should not be touched.
Only actual code should be reindented.
"""
            return (sys.path, some_string)


def example4(x: list):  # noqa : too complex function
    result = ''
    for y in x:
        for z in y:
            if (z == 'a'):
                result = 'A'
            elif (z == 'b'):
                result = 'B'
            elif (z == 'c'):
                result = 'C'
            elif (z == 'd'):
                result = 'D'
            elif (z == 'e'):
                result = 'E'
            elif (z == 'f'):
                result = 'F'
            elif (z == 'g'):
                result = 'G'
            elif (z == 'h'):
                result = 'H'
            else:
                result = 'X'

    return result
